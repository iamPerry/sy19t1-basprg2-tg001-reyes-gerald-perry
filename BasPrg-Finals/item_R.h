#pragma once
#include "Gacha_Item.h"

class item_R : public Gacha_Item
{
public:
	item_R();
	~item_R();

	void effect(int& hp, int& crystal, int& pts) override;
private:
	int mRarityPTS = 1;
};

