#pragma once
#include "Gacha_Item.h"

class item_Crystal : public Gacha_Item
{
public:
	item_Crystal();
	~item_Crystal();

	void effect(int& hp, int& crystal, int& pts) override;

private:
	const int addCrystal = 15;
};

