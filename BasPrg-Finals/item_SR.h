#pragma once
#include "Gacha_Item.h"

class item_SR : public Gacha_Item
{
public:
	item_SR();
	~item_SR();

	void effect(int& hp, int& crystal, int& pts) override;
	
private:
	int mRarityPTS = 10;
};

