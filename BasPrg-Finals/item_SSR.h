#pragma once
#include "Gacha_Item.h"

class item_SSR : public Gacha_Item
{
public:
	item_SSR();
	~item_SSR();

	void effect(int& hp, int& crystal, int& pts) override;

private:
	int mRarityPTS = 50;
};

