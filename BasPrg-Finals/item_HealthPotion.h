#pragma once
#include "Gacha_Item.h"

class item_HealthPotion : public Gacha_Item
{
public:
	item_HealthPotion();
	~item_HealthPotion();

	void effect(int& hp, int& crystal, int& pts) override;

private:
	const int heal = 30;
};

