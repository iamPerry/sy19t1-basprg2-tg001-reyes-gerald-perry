#include <iostream>
#include <string>
#include <time.h>
#include <vector>

#include "Gacha_Item.h"
#include "item_SSR.h"
#include "item_SR.h"
#include "item_R.h"
#include "item_HealthPotion.h"
#include "item_Bomb.h"
#include "item_Crystal.h"

using namespace std;

void display(int hp, int crystals, int pts, int pulls)
{
	cout << "HP:\t\t" << hp
		<< "\nCrystals:\t" << crystals
		<< "\nRarity Points:\t" << pts
		<< "\nPull count:\t" << pulls << endl;
}

Gacha_Item* pull()
{
	Gacha_Item* item;
	if (rand() % 100 < 1) { item = new item_SSR(); item->setName("SSR"); return item; }
	if (rand() % 100 < 10) { item = new item_SR(); item->setName("SR"); return item;}
	if (rand() % 100 < 25) { item = new item_HealthPotion(); item->setName("Health Potion"); return item;}
	if (rand() % 100 < 35) { item = new item_Crystal(); item->setName("Crystal"); return item;}
	if (rand() % 100 < 55) { item = new item_Bomb(); item->setName("Bomb"); return item;}
	else { item = new item_R(); item->setName("R"); return item;}

}

void countItems(vector<string> items)
{
	string gacha_items[6] = { "R", "SR", "SSR", "Health Potion", "Bomb", "Crystals" };
	
	cout << "\nItems Pulled:\n" << endl;

	for (int i = 0; i < 6; i++)
	{
		int count = 0;
		for (int j = 0; j < items.size(); j++)
			if (items[j] == gacha_items[i]) count++;

		cout << gacha_items[i] << " x" << count << endl;
	}
}

int main()
{
	srand(time(NULL));

	int crystals = 100;
	int rarityPTS = 0;
	int hp = 100;
	int pulls = 0;

	vector<string> item_list;
	

	while (crystals > 0 && hp > 0 && rarityPTS != 100)
	{
		display(hp, crystals, rarityPTS, pulls);
		
		crystals -= 5;
		Gacha_Item* item = pull();
		cout << "\nYou pulled: " << item->getName() << " item\n" << endl;

		item->effect(hp, crystals, rarityPTS);
		item_list.push_back(item->getName());

		pulls++;
		system("pause");
		system("cls");
	}

	if (hp <= 0 || crystals <= 0) cout << "YOU LOSE!\n" << endl;
	else cout << "YOU WIN!\n" << endl;

	display(hp, crystals, rarityPTS, pulls);
	countItems(item_list);

	system("pause");
	return 0;
}