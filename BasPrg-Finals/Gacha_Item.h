#include <string>
#pragma once

using namespace std;

class Gacha_Item
{
public:
	Gacha_Item();
	~Gacha_Item();

	string getName();
	void setName(string name);

	virtual void effect(int& hp, int& crystal, int& pts) = 0;
private:
	string mName;
};

