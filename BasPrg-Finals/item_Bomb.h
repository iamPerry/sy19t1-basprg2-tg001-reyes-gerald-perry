#pragma once
#include "Gacha_Item.h"

class item_Bomb : public Gacha_Item
{
public:
	item_Bomb();
	~item_Bomb();

	void effect(int& hp, int& crystal, int& pts) override;

private:
	const int damage = 25;
};

