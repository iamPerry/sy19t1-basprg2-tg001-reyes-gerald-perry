#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct node 
{
	string data;
	node* next = NULL;
	node* previous = NULL;
};

//THIS WILL INSERT NAMES ON A LINKED LIST
void insertNames(node* &head, node* &tail)
{
	string name;

	cout << "NIGHT WATCH MEMBER LIST" << endl;
	for (int i = 0; i < 5; i++) {
		node* temp = new node;

		//PROMPT THE USER TO ENTER MEMBER'S USERNAME
		cout << "Soldier " << i + 1 << ": ";
		cin >> name;

		temp->data = name;
		temp->next = NULL;

		//IF LIST IS EMPTY, THIS WILL INSERT FIRST NODE
		if (i == 0) {
			head = temp;
			tail = temp;
		}
		//THIS WILL INSERT THE NEXT NODE
		else {
			tail->next = temp;
			tail = temp;
		}
	}
}

//THIS WILL DISPLAY THE DATA OF THE LINKED LIST
void display(node* head) 
{
	cout << "MEMBERS LEFT:\t";
	while (head != NULL) {
		cout << head->data << ' ';
		head = head->next;
	}
	cout << endl;
}

//THE Lord.C WILL SELECT ONE MEMBER AS STARTING POINT
void selectStartPoint(node* head, node* &holder) 
{
	int startPoint = rand() % 5 + 1;

	cout << "LORD COMMANDER: LET'S START AT NUMBER ";
	for (int i = 0; i < startPoint; i++) {
		//AT STARTPOINT-1 MEANS LORD.C HAVE FINISHED COUNTING AND HAS CHOSEN THE LAST MEMBER IN THE COUNT
		if (i == (startPoint - 1)) {
			holder = head;
			cout << i + 1 << ". " << holder->data << " CHOOSE A NUMBER FROM 1 to 5" << endl;
		}
		//THIS WILL CONTINUE TO RUN UNTIL THE NUMBER BEFORE THE LAST WILL COME UP
		head = head->next;
	}
}

//RANDOMLY SELECT A NUMBER RANGING EQUAL TO THE NUMBER OF MEMBERS LEFT
int randSelect(node* head, node* holder) 
{
	int number = 0;
	int randNumber;

	//DETERMINE HOW MANY MEMBER ARE STILL IN THE CIRCLE
	while (head != NULL) {
		number++;
		head = head->next;
	}

	//WILL RANDOMIZE A NUMBER RANGING FROM 1 UP TO NUMBER OF MEMBERS
	randNumber = rand() % number + 1;
	cout << holder->data << ":\tLET'S COUNT " << randNumber << endl;
	return randNumber;
}

//THIS WILL PASS THE CLOAK BASE ON THE RETURNED VALUE OF randSelect()
void passCloak(node* head, node* tail, node* &holder, int count) 
{
	cout << "\n. . . . .Passing the cloak. . . . .\n" << endl;

	for (int i = 1; i < count; i++) {
		//MEMBER WILL COUNT
		cout << holder->data << ":\t" << i << endl;
		//IF CLOAK WAS PASSED TO THE LAST MEMBER, IT WILL BE PASSED TO THE FIRST MEMBER
		if (holder == tail) { holder = head; }
		//IF NOT, THE CLOAK WILL BE PASSED TO THE NEXT MEMBER
		else { holder = holder->next; }
	}
	//PRINTS THE LAST HOLDER
	cout << holder->data << ":\t" << count << endl;
	cout << "\nTHE LAST HOLDER OF THE CLOAK IS " << holder->data << endl;

	//TEMPORARY HOLD THE DATA OF THE LAST HOLDER TO BE STORED TO PREVIOUS
	node* temp = holder;

	//IF THE LAST HOLDER IS THE TAIL, THE NEXT HOLDER WILL BE THE HEAD
	if (holder == tail) { holder = head; }
	//IF NOT, THE NEXT HOLDER WILL BE THE NEXT MEMBER
	else{ holder = holder->next; }

	//THIS WILL SERVE AS "KEY"
	holder->previous = temp;

	cout << holder->previous->data << " WILL NOW BE REMOVED FROM THE CIRCLE. \nTHE HOLDER OF THE CLOAK NOW IS " << holder->data << endl;

	cout << endl;
	system("pause");
	system("cls");
}

//REMOVES MEMBER FROM THE LIST AND RELINKS THE LIST
void removeMember(node* &head, node* holder) {
	node* temp = head;
	node *prev = temp;
	
	//THIS WILL CHECK THE FIRST NODE ONLY
	if (head->data == holder->previous->data) {
		head = temp->next;
		free(temp);
		return;
	}
	else {
		//THIS WILL CHECK THE NEXT NODE UNTIL THE LAST NODES
		while (temp->data != holder->previous->data) {
			prev = temp;
			temp = temp->next;
		}
		prev->next = temp->next;
		free(temp);
	}
}
	
int main() {
	srand(time(NULL));
	//THIS INDICATES THAT THE LIST IS EMPTY
	node* head = NULL;
	node* tail = NULL;
	node* holder;
	int count;

	insertNames(head, tail);
	system("cls");
	display(head);

	//THE LORD COMMANDER WILL NOW PICK A STARTING POINT
	selectStartPoint(head, holder);

	cout << endl;
	system("pause");
	system("cls");

	//THIS WILL RUN UNTIL THERE IS ONLY ONE MEMBER LEFT
	for (int i = 0; i < 4; i++) {
		//RANDOMIZE A NUMBER BASE ON THE NUMBER OF MEMBERS LEFT
		count = randSelect(head, holder);
		//DETERMINE WHO WILL BE REMOVE AND WHO WILL HOLD THE CLOAK NEXT
		passCloak(head, tail, holder, count);
		//REMOVE THE MEMBER FROM THE CIRCLE THEN RELINK THE LIST
		removeMember(head, holder);
		//DISPLAY THE UPDATED LIST
		display(head);
	}
	system("pause");
	system("cls");
	cout << "LORD COMMANDER: " << head->data << " YOU HAVE BEEN CHOSEN TO SEEK HELP FROM THE SEVEN KINGDOMS! MAKE HASTE!\n" << endl;


	system("pause");
	return 0;
}