#include <iostream>
#include <string>
#include "unit.h"

using namespace std;

unit::unit()
{
}

unit::~unit()
{
}

string unit::getName()
{
	return mName;
}

string unit::getClass()
{
	return mClass;
}

int unit::getHp()
{
	return mHp;
}

int unit::getPow()
{
	return mPow;
}

int unit::getVit()
{
	return mVit;
}

int unit::getDex()
{
	return mDex;
}

int unit::getAgi()
{
	return mAgi;
}

void unit::setStats(string name, string unitClass, int hp, int pow, int vit, int dex, int agi)
{
	mName = name;
	mClass = unitClass;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mDex = dex;
	mAgi = agi;
}

void unit::statIncrease(int hp, int pow, int vit, int dex, int agi)
{
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mDex = dex;
	mAgi = agi;
}

void unit::viewStats()
{
	cout << "Name:\t" << mName
		<< "\nClass:\t" << mClass
		<< "\nHP:\t" << mHp
		<< "\nPow\t" << mPow
		<< "\nVit:\t" << mVit
		<< "\nDex:\t" << mDex
		<< "\nAgi:\t" << mAgi << endl;
}

float unit::attack(int enemyVit, float bonusDmg)
{
	float damage = (mPow - enemyVit) * bonusDmg;

	if (damage <= 0) damage = 1;
	return damage;
}

float unit::hitrate(int enemyAgi)
{
	return ((float)mDex / enemyAgi) * 100;
}

void unit::takeDamage(int enemyDamage)
{
	mHp = mHp - enemyDamage;
}
