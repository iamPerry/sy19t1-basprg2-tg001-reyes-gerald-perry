#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include "unit.h"

using namespace std;

//WARRIOR BASE STATS
const int WAR_BASE_HP = 20;
const int WAR_BASE_POW = 8;
const int WAR_BASE_VIT = 10;
const int WAR_BASE_AGI = 5;
const int WAR_BASE_DEX = 5;

//ASSASSIN BASE STATS
const int ASS_BASE_HP = 15;
const int ASS_BASE_POW = 10;
const int ASS_BASE_VIT = 5;
const int ASS_BASE_AGI = 8;
const int ASS_BASE_DEX = 5;

//MAGE BASE STATS
const int MGE_BASE_HP = 10;
const int MGE_BASE_POW = 10;
const int MGE_BASE_VIT = 5;
const int MGE_BASE_AGI = 5;
const int MGE_BASE_DEX = 8;

string inputName()
{
	string name;

	cout << "Greetings Adventurer! What is your name? ";
	getline(cin, name);

	return name;
}

string pickClass()
{
	int pick;

	cout << "Choose your class"
		<< "\n\t[1] Warrior"
		<< "\n\t[2] Assassin"
		<< "\n\t[3] Mage"
		<< "\nClass:\t";
	cin >> pick;

	if (pick == 1) return "Warrior";
	if (pick == 2) return "Assassin";
	if (pick == 3) return "Mage";
}

unit* setBaseStats(string name, string pickedClass)
{
	unit* unitClass = new unit;

	if (pickedClass == "Warrior") unitClass->setStats(name, pickedClass, WAR_BASE_HP, WAR_BASE_POW, WAR_BASE_VIT, WAR_BASE_DEX, WAR_BASE_AGI);
	if (pickedClass == "Assassin") unitClass->setStats(name, pickedClass, ASS_BASE_HP, ASS_BASE_POW, ASS_BASE_VIT, ASS_BASE_DEX, ASS_BASE_AGI);
	if (pickedClass == "Mage") unitClass->setStats(name, pickedClass, MGE_BASE_HP, MGE_BASE_POW, MGE_BASE_VIT, MGE_BASE_DEX, MGE_BASE_AGI);

	return unitClass;
}

unit* generateNewEnemy(int stage)
{
	unit* enemy = new unit();

	float multiplier = 0.5 * stage;

	int hp = rand() % (10 + (10 * (int)multiplier)) + (5 + (5* (int)multiplier));
	int pow = rand() % (10 + (10 * (int)multiplier)) + (5 + (5* (int)multiplier));
	int vit = rand() % (5 + (5 * (int)multiplier)) + (1 + (1* (int)multiplier));
	int dex = rand() % (5 + (5 * (int)multiplier)) + (1 + (1 * (int)multiplier));
	int agi = rand() % (5 + (5 * (int)multiplier)) + (1 + (1 * (int)multiplier));


	//This will determine what will be the class of the enemy
	// [1] Warrior
	// [2] Assassin
	// [3] Mage
	int picker = rand() % 3 + 1;

	//This will increase the stats of the enemy everytime the player advances each stage
	if (picker == 1) enemy->setStats("Enemy Warrior", "Warrior", hp, pow, vit, dex, agi);
	if (picker == 2) enemy->setStats("Enemy Assassin", "Assassin", hp, pow, vit, dex, agi);
	if (picker == 3) enemy->setStats("Enemy Mage", "Mage", hp, pow, vit, dex, agi);

	return enemy;
}

void battleResult(unit* player, unit* enemy)
{
	if (enemy->getHp() <= 0)
	{
		//Player will be healed by 5%
		int hp = player->getHp() + round((player->getHp() * 0.3));
		int pow = player->getPow();
		int vit = player->getVit();
		int dex = player->getDex();
		int agi = player->getAgi();

		cout << player->getName() << " WON! STATS INCREASED" << endl;
		cout << "HP is healed by " << round(player->getHp() * 0.3) << endl;
		player->viewStats();

		cout << "\nTO\n" << endl;

		if (enemy->getClass() == "Assassin") player->statIncrease(hp, pow, vit, dex + 3, agi + 3);
		if (enemy->getClass() == "Mage") player->statIncrease(hp, pow + 5, vit, dex, agi);
		if (enemy->getClass() == "Warrior") player->statIncrease(hp + 3, pow, vit + 3, dex, agi);

		player->viewStats();
	}

	if (player->getHp() <= 0) cout << player->getName() << " have no more hp left" << endl;
	system("pause");
}

void battle(unit* player, unit* enemy)
{
	int attack;
	float bonusDmg;

	while (player->getHp() > 0 && enemy->getHp() > 0)
	{
		//PLAYER TURN
		if ((player->getClass() == "Warrior" && enemy->getClass() == "Assassin") ||
			(player->getClass() == "Assassin" && enemy->getClass() == "Mage") ||
			(player->getClass() == "Mage" && enemy->getClass() == "Warrior")) bonusDmg = 1.5;
		
		else bonusDmg = 1;

		//check if enemy can evade
		if (rand() % 100 < player->hitrate(enemy->getAgi()))
		{
			attack = player->attack(enemy->getVit(), bonusDmg);
			enemy->takeDamage(attack);

			cout << enemy->getName() << " took " << attack << " damage from " << player->getName() << endl;
			cout << player->getName() << ":\t" << player->getHp() << endl;
			cout << enemy->getName() << ":\t" << enemy->getHp() << endl;
		}
		else cout << player->getName() << " attack missed!" << endl;
		system("pause");
		cout << endl;
		if (enemy->getHp() <= 0) break;

		//ENEMY TURN
		if ((enemy->getClass() == "Warrior" && player->getClass() == "Assassin") ||
			(enemy->getClass() == "Assassin" && player->getClass() == "Mage") ||
			(enemy->getClass() == "Mage" && player->getClass() == "Warrior")) bonusDmg = 1.5;
		else bonusDmg = 1;

		//check if enemy can evade
		if (rand() % 100 < enemy->hitrate(player->getAgi()))
		{
			attack = enemy->attack(player->getVit(), bonusDmg);
			player->takeDamage(attack);

			cout << player->getName() << " took " << attack << " damage from " << enemy->getName() << endl;
			cout << player->getName() << ":\t" << player->getHp() << endl;
			cout << enemy->getName() << ":\t" << enemy->getHp() << endl;
		}
		else cout << enemy->getName() << " attack missed!" << endl;
		system("pause");
		cout << endl;
		if (player->getHp() <= 0) break;
	}
	//If match ended, show battle results. 
	//If player won stats will increase
	system("cls");
	cout << "BATTLE ENDED" << endl;
	battleResult(player, enemy);
}



int main()
{
	srand(time(NULL));
	int stage = 1;

	string name = inputName();
	string unitClass = pickClass();
	unit* player = setBaseStats(name, unitClass);
	unit* enemy = new unit();

	while (player->getHp() > 0)
	{
		system("cls");
		cout << "STAGE: " << stage << endl;
		player->viewStats();
		

		cout << "\nVS\n" << endl;
		enemy = generateNewEnemy(stage);
		enemy->viewStats();

		system("pause");
		system("cls");
		battle(player, enemy);
		stage++;
	};
	
	system("cls");
	cout << "GAME OVER"
		<< "\nStage cleared:\t" << stage << endl;
	player->viewStats();

	system("pause");
	return 0;
}