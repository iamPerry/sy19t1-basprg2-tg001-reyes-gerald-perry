#include <string>
#include <iostream>
#pragma once

using namespace std;

class unit
{
public:
	unit();
	~unit();
	
	string getName();
	string getClass();
	int getHp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();

	void setStats(string name, string unitClass, int hp, int pow, int vit, int dex, int agi);
	void statIncrease(int hp, int pow, int vit, int dex, int agi);
	void viewStats();
	
	float attack(int enemyVit, float bonusDmg);
	float hitrate(int enemyAgi);
	void takeDamage(int enemyDamage);

private:
	string mName;
	string mClass;
	int mHp;
	int mPow;
	int mVit;
	int mDex;
	int mAgi;

};

