#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

void sort(int sortArray[]) {
	int temp;
	for (int x = 0; x<7; x++) {
		for (int y = x + 1; y<7; y++) {
			if (sortArray[x] > sortArray[y]) {
				temp = sortArray[x];
				sortArray[x] = sortArray[y];
				sortArray[y] = temp;
			}
		}
	}
}

bool purchasable(int price, int gold, int packages[]) {
	int sum = packages[6] + gold;
	if (price > sum) {
		return false; //the price of the item is higher than the highest package + gold of the player
	}
	else
		return true;
}

int purchasePackage(int price, int gold, int packages[]) {
	string buy;

	cout << "Get more gold!!! Get our packages now!!!" << endl;
	for (int j = 0; j<7; j++) {
		cout << "Package " << j + 1 << ":\t" << packages[j] << endl;
	}
	cout << endl;
	for (int i = 0; i<7; i++) {
		if ((packages[i] + gold) > price) {
			cout << "Do you want to buy our Package " << i + 1 << " worth " << packages[i] << " to gain sufficient gold? (yes/no): ";
			cin >> buy;
			system("cls");

			if (buy == "yes") {
				gold = gold + packages[i];
				cout << "Purchase successful! Your total gold is " << gold << endl;
			}
			else if (buy == "no") {
				cout << "Please input another item price or quit shopping." << endl;
			}
			break;
		}
	}
	return gold;
}

void printItems(string itemList[], int itemPrice[]) {
	cout << "SHOPKEEPER's ITEMS" << endl;
	for (int x = 0; x < 7; x++) {
		cout << '(' << x + 1 << ") " << itemList[x] << "( " << itemPrice[x] << " )" << endl;
	}
}

int main() {
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int key, itemPrice, gold = 250;
	string items[] = { "Red Potion", "Blue Potion", "Evolver Stone", "Skill Reset", "Stat Reset", "Character Reset", "Flaming Sword of Vengeance"};
	int prices[] = { 100, 100, 1000, 100000, 100000, 175000, 500000 };
	string cont;
	bool check;

	sort(packages);

	do {
		system("cls");

		//PRINT SHOP ITEMS AND THEIR PRICES
		printItems(items, prices);

		//WILL PROMPT USER TO ENTER THE NUMBER OF ITEM CHOICE
		cout << "Hello Adventurer! What item are you buying? Key in the number (Gold in hand: " << gold << "): ";
		cin >> key;
		itemPrice = prices[key - 1];

		system("cls");
		//IF GOLD IS ENOUGH TO BUY THE ITEM IT WILL PURCHASE IMMIDIATELY
		if (itemPrice < gold) {
			gold = gold - itemPrice;
			cout << "Purchase Successful! You brough the item " << items[key-1] << " for " << itemPrice << ". Your remaining gold is " << gold << endl;
		}
		//THIS WILL CHECK IF THERE ARE PURCHASABLE PACKAGES ENOUGH TO BUY THE ITEM
		else {
			cout << "Insufficient gold adventurer. ";
			check = purchasable(itemPrice, gold, packages);
			if (check) {
				gold = purchasePackage(itemPrice, gold, packages);
			}
			else {
				cout << "There are no available packages to avail the item." << endl;
			}
		}

		cout << "Do you want to continue shopping? (yes/no): ";
		cin >> cont;
	} while (cont != "no");

	cout << "Thank you for shopping adventurer!" << endl;

	system("pause");

	return 0;
}