#include <iostream>
#include <conio.h>
#include <time.h>

using namespace std;

struct item{
	string name;
	int gold;
}lootedItem;

//EXERCISE 3-4
item generateItem() {
	item loot;
	string itemName[5] = { "Mithril Ore", "Sharp Talon", "Thick Leather" , "Jellopy", "Cursed Stone" };
	int itemPrice[5] = { 100, 50, 25, 5, 0 };
	int randomize;

	//GENERATE A RANDOM NUMBER FROM 1 TO 5 TO DETERMINE THE LOOT ITEM
	randomize = rand() % 5;
	loot.name = itemName[randomize];
	loot.gold = itemPrice[randomize];

	return loot;
}

//EXERCISE 3-5
void enterDungeon(int& gold) {
	char cont;
	int totalLootGold = 0, multiplier = 1;
	
	//UPON ENTERING DUNGEON GOLD WILL BE DEDUCTED BY 25
	gold = gold - 25;

	do {
		system("cls");
		//CALL OUT FUNCTION GENERATE ITEM TO LOOT
		lootedItem = generateItem();

		//CHECK IF THE LOOTED ITEM IS A CURSED STONE
		if(lootedItem.name == "Cursed Stone") { 
			cout << "You looted a Cursed stone! You will lose all your looted gold and will exit the dungeon now." << endl;
			system("pause");
			//SET THE TOTAL LOOT GOLD TO 0 AND BREAK THE LOOP
			totalLootGold = 0; break;
		}
		//IF THE LOOTED ITEM IS NOT A C.STONE THE CODE BELOW WILL RUN
		//PRINT OUT THE NAME OF LOOTED ITEM AND THE PRICE x MULTIPLIER
		cout << "You looted a " << lootedItem.name
			<< " worth " << lootedItem.gold << " (x" << multiplier << ')' << endl;

		//THIS WILL COMPUTE THE TOTAL LOOTED GOLD COLLECTED DURING THE INSTANCE
		totalLootGold = totalLootGold + (lootedItem.gold * multiplier);
		cout << "Total looted gold:\t" << totalLootGold << endl;

		//ASK THE USER IF HE STILL WANTS TO CONTINUE LOOTING
		cout << "\nPress any key to continue looting or press \"x\" to exit dungeon";
		cont = _getch();

		multiplier++;
		if (multiplier >= 4) { multiplier = 4; }

	} while (cont != 'x');

	//UPON EXITING THE DUNGEON THE LOOTED GOLD WILL BE ADDED TO THE PLAYER'S CURRENT GOLD
	gold = gold + totalLootGold;
}

//EXERCISE 3-6
int main() {
	srand(time(NULL));
	int playerGold = 50;

	//CHECK IF THE PLAYER HAS NO MORE GOLD, UNABLE TO PAY THE DUNGEON FEE OR HAVE MORE THAN 500 GOLD
	while ((playerGold >= 25) && (playerGold < 500)) {
		system("cls");

		cout << "================DUNGEON LOOT MINI GAME================"
			<< "\n\n-You will need to pay 25 gold to enter the dungeon"
			<< "\n-Exciting items awaits inside the dungeon"
			<< "\n-Upon exiting the dungeon, all looted items will be added to your gold"
			<< "\n\nPRESS ANY KEY TO ENTER THE DUNGEON. YOUR TOTAL GOLD:\t" << playerGold; _getch();
		enterDungeon(playerGold);
	};

	system("cls");

	//CHECK IF THE PLAYER WON OR LOST THE GAME
	if (playerGold >= 500) {
		cout << "OVER 500 GOLD WIN LIMIT ACHIEVED! YOU HAVE " << playerGold << " GOLD. THANK YOU FOR PLAYING!" << endl;
	}
	else if(playerGold <= 0) { cout << "YOU DON'T HAVE GOLD ANY MORE! GAME OVER!!!\n" << endl; }
	else { cout << "YOU DON'T HAVE ANY MORE GOLD TO ENTER THE DUNGEON. GAME OVER!!!\n" << endl; }
	
	system("pause");
	return 0;
}
