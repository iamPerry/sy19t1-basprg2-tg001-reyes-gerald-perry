#include <iostream>
#include <time.h>

using namespace std;

//EXERCISE 3-2 and 3-3
int *createDArray(int n){
	int* numbers = new int[n];

	cout << "GENERATED ARRAY: ";
	//THIS WILL GENERATE RANDOM NUMBERS RANGING FROM 1-100
	for (int i = 0; i < n; i++) {
		numbers[i] = rand() % 100 + 1;
		cout << numbers[i] << ' ';
	}

	cout << endl;

	return numbers;
}


int main() {
	int* fillArray;
	int size;

	cout << "ENTER SIZE OF ARRAY: ";
	cin >> size;

	fillArray = createDArray(size);
	delete[] fillArray;
	cout << endl;

	system("pause");
	return 0;
}