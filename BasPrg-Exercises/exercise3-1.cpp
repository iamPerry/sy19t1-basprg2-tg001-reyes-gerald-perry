#include <iostream>
#include <time.h>

using namespace std;

//EXERCISE 3-1
void randomize(int* numbers) {
	//THIS WILL FILL THE ARRAY 10 RANDOMIZED NUMBERS RANGING FROM 1-100
	cout << "GENERATED ARRAY: ";
	for (int i = 0; i < 10; i++) {
		numbers[i] = rand() % 100 + 1;
		cout << numbers[i] << ' ';
	}
	cout << endl;
}

int main() {
	srand(time(NULL));
	int fillArray[10];

	randomize(fillArray);

	system("pause");
	return 0;
}