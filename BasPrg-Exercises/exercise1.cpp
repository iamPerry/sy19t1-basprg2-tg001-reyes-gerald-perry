#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void factorial(int& sum, int num) {
	for (int i = 0; i < num; i++) {
		sum = sum * (i + 1);
	}
}
void printArray(string myArray[]) {
	
	cout << "Items in the inventory:\n" << endl;
	for (int i = 0; i < 8; i++) {
		cout << myArray[i] << endl;
	}
}
void instance(string items[], string item) {
	int quantity = 0;

	for (int i = 0; i < 8; i++) {
		if (item == items[i]) {
			quantity++;
		}
	}
	cout << "Item " << item << " has quantity of " << quantity << endl;
}
void randomize(int randArray[]) {
	cout << "Randomized array: ";
	for (int i = 0; i < 10; i++) {
		randArray[i] = rand() % 100 + 1;
		cout << randArray[i] << ' ';
	}
	cout << endl;
}
void findLargest(int& biggest, int randArray[]) {
	cout << "Randomized array: ";
	for (int i = 0; i < 10; i++) {
		randArray[i] = rand() % 100 + 1;
		cout << randArray[i] << ' ';
		if (randArray[i] > biggest) {
			biggest = randArray[i];
		}
	}
	cout << endl;
}

void ex1_1() {
	system("cls");
	int number, result = 1;

	cout << "Enter number: ";
	cin >> number;

	if (number == 0) {
		result = 1;
	}
	else {
		factorial(result, number);
	}
	cout << "The factorial of " << number << " is " << result << endl;
}

void ex1_2() {
	system("cls");
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	
	printArray(items);
}

void ex1_3() {
	system("cls");
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	string find;

	cout << "Enter name of item: ";
	cin.ignore();  getline(cin, find);
	instance(items, find);

}

void ex1_4() {
	system("cls");
	int numbers[10];

	randomize(numbers);
}

void ex1_5() {
	system("cls");
	int numbers[10], large = 0;

	findLargest(large, numbers);
	cout << "The largest number is " << large << endl;
}

int main() {
	srand(time(NULL));
	int exe;

	cout << "(1) Factorial\n"
		<< "(2) Print Array\n"
		<< "(3) Number of Instances\n"
		<< "(4) Random Array Fill\n"
		<< "(5) Largest Number\n\n"
		<< "Execute exercise #: ";
	cin >> exe;

	switch (exe) {
	case 1: ex1_1(); break;
	case 2: ex1_2(); break;
	case 3: ex1_3(); break;
	case 4: ex1_4(); break;
	case 5: ex1_5(); break;
	}

	system("pause");
	return 0;
}