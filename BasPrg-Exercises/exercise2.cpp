#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

struct roll {
	int dice[2];
	int value;
}player, dealer;

//EXERCISE 2-1
int bet_ver_1(int& gold){
	int bet;

	//PLAYER CAN ONLY BET MORE THAN 0 OR LESS THAN REMAINING GOLD
	do {
		cout << "INPUT BET HERE (GOLD: " << gold << "): ";
		cin >> bet;
	} while ((bet != 0) && (bet > gold));
	
	//BET WILL IMMEDIATELY DEDUCT PLAYER'S GOLD
	gold = gold - bet;

	return bet;
}
void bet_ver_2(int& gold, int&bet){
	//PLAYER CAN ONLY BET MORE THAN 0 OR LESS THAN REMAINING GOLD
	do {
		cout << "INPUT BET HERE (GOLD: " << gold << "): ";
		cin >> bet;
	} while ((bet == 0) && (bet > gold));

	//BET WILL IMMEDIATELY DEDUCT PLAYER'S GOLD
	gold = gold - bet;
}

//EXERCISE 2-2
void rollDice(roll& roller) {
	for (int i = 0; i < 2; i++) {
		roller.dice[i] = rand() % 6 + 1;
		
		cout << "DICE " << i+1 << ": " << roller.dice[i] << endl;
	}
	roller.value = roller.dice[0] + roller.dice[1];
}

//EXERCISE 2-3
void payout(roll player, roll dealer, int bet, int& gold) {
	int pay = 0;

	//IF PLAYER AND DEALER ROLLED THE SAME DICE HIS BET WILL RETURN
	if (player.value == dealer.value) { pay = 0; cout << "\nROUND IS A DRAW" << endl; }

	//IF PLAYER ROLLS A SNAKE EYES HE WINS 3X HIS BET
	else if (player.value == 2) { pay = bet * 3; cout << "\nPLAYER WON 3x HIS BET" << endl;	}

	//IF DEALER ROLLS A HIGHER DICE, PLAYER LOSES HIS BET
	else if ((player.value < dealer.value) || (dealer.value == 2)) { pay = -bet; cout << "\nPLAYER LOST" << endl;}

	//IF PLAYER ROLLS A HIGHER DICE HE WIN BACK HIS BET PLUS THE AMOUNT HE BET
	else if (player.value > dealer.value) { pay = bet; cout << "\nPLAYER WINS" << endl; }

	cout << "PAYOUT:\t" << pay << endl;
	//PAYOUT WILL BE ADDED TO THE GOLD
	gold = gold + bet + pay;
	cout << "GOLD:\t" << gold << endl;
}

//EXERCISE 2-4
void playRound(int& gold) {
	int bet;

	system("cls");
	//PLAYER WILL BET
	bet = bet_ver_1(gold);

	system("cls");

	//PLAYER WILL ROLL DICE
	cout << "PLAYER DICE" << endl;
	rollDice(player);
	//DEALER WILL ROLL DICE
	cout << "\nDEALER DICE" << endl;
	rollDice(dealer);
	//COMPUTES PAYOUT IF WIN or LOSE
	payout(player, dealer, bet, gold);
}

int main() {
	srand(time(NULL));
	int gold = 1000;
	char playAgain;
	
	//GAME WILL END IF PLAYER HAS NO MORE GOLD OR IF HE WANTS TO QUIT
	do {
		system("cls");
		playRound(gold);
		cout << "Press any key to play again or press \"x\" to quit. " << endl;
		playAgain = _getch();
	} while ((gold > 0) && (playAgain != 'x'));

	system("cls");
	if (gold <= 0) { cout << "YOU HAVE NO MORE GOLD. GAME OVER" << endl; }
	cout << "THANK YOU FOR PLAYING!" << endl;

	system("pause");
	return 0;
}