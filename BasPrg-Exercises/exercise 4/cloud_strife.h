#pragma once
#include <string>
#include <vector>
using namespace std;

class cloud_strife
{
public:
	int lvl;
	int hp;
	int mp;
	int exp;
	int nextLevel;
	int limitLevel;

	int str;
	int dex;
	int vit;
	int magic;
	int spirit;
	int luck;

	int atk;
	int atkPercentage;
	int def;
	int defPercentage;
	int mgcAtk;
	int mgcDef;
	int mgcDefPercentage;

	int atkMultiplier;
	string eqpWeap;
	string eqpArm;
	string eqpAcc;
	vector<string> eqpMateria;
	vector<string> eqpMagic;

	//basic battle options
	void attack();
	void defend();
	void useItem();
	void magic();

	//cloud's list of limit breaks
	void LB_Braver();
	void LB_Climhazzard();
	void LB_XSlash();
	void LB_BladeBeam();
	void LB_Meteorain();
	void LB_FinishingTouch();
	void LB_OmniSlash();


	//enemy attacking cloud
	void takeDamage();
	void statusAilment();
};

