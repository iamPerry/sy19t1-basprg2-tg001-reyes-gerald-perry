#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

struct deck
{
	vector<string> cards;
	string cardPick;
}kaiji, enemy;

//GENERATES CARDS ON HAND
deck generateCards(string side)
{
	deck hand;
	int randPosition = rand() % 5;

	vector<string> cards;
	for (int i = 0; i < 5; i++)
	{
		//parameter "side" is either emperor or slave and will be pushed on the vector
		if (i == randPosition) { cards.push_back(side); }
		else { cards.push_back("civilian"); }
	}
	hand.cards = cards;

	return hand;
}

//this will prompt kaiji to wager distance
int mmWager(int distance)
{
	int mm;

	//this will loop if kaiji bets below 1mm or over 30mm
	do {
		cout << "How many millimeters will you bet Kaiji?"
			<< "\n(You can only bet from 1mm - " << 30 - distance << "mm): ";
		cin >> mm;
	} while (mm < 1 || mm > 30 - distance);

	return mm;
}

//kaiji will change decks after 3 rounds
//this will return the multiplier of 100k/500k depending on cards kaiji is holding
int pickDeck(deck& kaiji, deck& enemy, int round)
{
	//this will generate the EMPEROR CARDS for kaiji from rounds 1-3 and rounds 7-9 
	if (round <= 3 || (round >= 7 && round <= 9))
	{
		cout << "KAIJI'S DECK: EMPEROR"
			<< "\nENEMY'S DECK: SLAVE" << endl;
		kaiji = generateCards("emperor");
		enemy = generateCards("slave");
		return 100000;
	}
	//this will generate the SLAVE CARDS for kaiji from rounds 4-6 and rounds 10-12 
	else
	{
		cout << "KAIJI'S DECK: SLAVE"
			<< "\nENEMY'S DECK: EMPEROR" << endl;
		kaiji = generateCards("slave");
		enemy = generateCards("emperor");
		return 500000;
	}
}

//compare the cards played by both players
//will return true/false to determine if kaiji will win or lose
bool comparePick(string kaiji, string enemy)
{
	//if both played CIVILIAN
	if (kaiji == enemy)
	{
		cout << "DRAW!!!" << endl;
		system("pause");
		return false;
	}
	else if (kaiji == "emperor") {
		if (enemy == "slave") { return false; }
		else { return true; }
	}
	else if (kaiji == "slave")
	{
		if (enemy == "civilian") { return false; }
		else { return true; }
	}
	else if (kaiji == "civilian")
	{
		if (enemy == "emperor") { return false; }
		else { return true; }
	}
}

void playRound(int round, int& money, int& distance)
{
	//mm = has value of the waged distance
	//multiplier = will be multiplied to the waged distance
	int mm, multiplier;
	bool win;

	//ask first how much distance kaiji will wager during the round
	mm = mmWager(distance);
	system("cls");

	cout << "========ROUND " << round << "===========" << endl;
	cout << "Kaiji wagerd " << mm << "mm on this round." << endl;
	//This will determine what cards the player will have each round
	multiplier = pickDeck(kaiji, enemy, round);

	//the round will end if neither of the two played an emperor/slave card
	while ((kaiji.cardPick != "emperor" && kaiji.cardPick != "slave") &&
		(enemy.cardPick != "emperor" && enemy.cardPick != "slave"))
	{
		//the players will now play their cards
		cout << "\nPLACE YOUR CARDS!\n" << endl;

		//this will print kaiji's hand
		cout << "YOUR CARD ON HAND: ";
		for (size_t i = 0; i < kaiji.cards.size(); i++) {
			cout << kaiji.cards[i] << " ";
		}

		//kaiji will play a card
		cout << "\nPick a card: ";
		cin >> kaiji.cardPick;

		//this will remove the card from hand
		for (size_t k = 0; k < kaiji.cards.size(); k++) {
			if (kaiji.cardPick == kaiji.cards[k])
			{
				kaiji.cards.erase(kaiji.cards.begin() + k);
				break;
			}
		}

		//Enemy will play a card
		cout << "\nEnemy is picking a card. . . ." << endl;
		//randomize from 1 up to remaining cards of enemy
		enemy.cardPick = enemy.cards[(rand() % enemy.cards.size())];

		//this will remove the card from enemy's hand
		for (size_t e = 0; e < enemy.cards.size(); e++)
		{
			if (enemy.cardPick == enemy.cards[e])
			{
				enemy.cards.erase(enemy.cards.begin() + e);
				break;
			}
		}

		system("pause");
		system("cls");

		cout << "KAIJI'S CARD:\t" << kaiji.cardPick
			<< "\nENEMY'S CARD:\t" << enemy.cardPick << endl;

		//this will compare the cards picked by both players
		win = comparePick(kaiji.cardPick, enemy.cardPick);
	}
	//if kaiji wins the round, he will win his payout
	//if not, the mm will be added to distance
	if (win)
	{
		money = money + (mm * multiplier);
		cout << "\nYou won this round Kaiji! Your payout is " << mm * multiplier << " pericas. \nYou now have " << money << " pericas." << endl;
	}
	else
	{
		distance = distance + mm;
		cout << "\nYou lose this round Kaiji! The contraption will move " << mm << "mm closer.\nYou now have " << 30 - distance
			<< "mm left before your ear gets crushed." << endl;
	}
	system("pause");
	system("cls");
}

//Determin what kind of ending kaiji got
void ending(int money, int distance)
{
	if (distance >= 30)
	{
		cout << "Kaiji you had a BAD ENDING! Say good bye to your ear." << endl;
	}
	else if (money >= 20000000)
	{
		cout << "Congratulations kaiji! You survive and won " << money << "! You had a BEST ENDING" << endl;
	}
	else if (money < 20000000)
	{
		cout << "You only got " << money << " Perica but still you had your ear attached. You had a MEH ENDING" << endl;
	}
}

int main() {
	srand(time(NULL));
	int round = 1, money = 0, distance = 0;

	//this will run for 12 rounds even kaiji wins 20k
	//this will only stop if the contraption reaches kaiji's ear
	while (round <= 12 && distance < 30) {
		cout << "Round:\t\t\t" << round
			<< "\nPerica:\t\t\t" << money
			<< "\nDistance to ear:\t" << distance << "mm\n" << endl;
		playRound(round, money, distance);
		round++;
	};
	ending(money, distance);

	system("pause");
	return 0;
}